package assignments.assignment3;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    static ElemenFasilkom[] daftarElemenFasilkom = new ElemenFasilkom[100];

    static MataKuliah[] daftarMataKuliah = new MataKuliah[100];

    static int totalMataKuliah = 0;

    static int totalElemenFasilkom = 0; 

    public static ElemenFasilkom searchElemen(String nama) {
        for (ElemenFasilkom elemenFasilkom : daftarElemenFasilkom) {
            if(elemenFasilkom != null && elemenFasilkom.toString().equals(nama)){
                return elemenFasilkom;
            }
        }return null;
    }//method untuk mencari elemen di dalam dafar elemen berdasarkan namanya

    public static MataKuliah searchMatkkul(String namaMataKuliah) {
        for (MataKuliah mataKuliah : daftarMataKuliah) {
            if(mataKuliah.toString().equals(namaMataKuliah)){
                return mataKuliah;
            }
        }return null;
    }//mencari objek matakuliah yang ada di daftar matakkuliah sesuai namanya

    public static void addMahasiswa(String nama, long npm) {
        Mahasiswa mhs = new Mahasiswa(nama, npm);
        daftarElemenFasilkom[totalElemenFasilkom++]=mhs;
        /* TODO: implementasikan kode Anda di sini */
    }//method menambahkan objek mahasiswa ke dalam program

    public static void addDosen(String nama) {
        Dosen dsn = new Dosen(nama);
        daftarElemenFasilkom[totalElemenFasilkom++]=dsn;;
        /* TODO: implementasikan kode Anda di sini */
    }//menambahkan objek dosen ke dalam program

    public static void addElemenKantin(String nama) {
        ElemenKantin kantin= new ElemenKantin(nama);
        daftarElemenFasilkom[totalElemenFasilkom++]=kantin;
        /* TODO: implementasikan kode Anda di sini */
    }//menambahkan objek elemen kantin ke dalam program

    public static void menyapa(String objek1, String objek2) {//mekanisme menyapa
        if (objek1.equals(objek2)) {//menolak operasi jika objek yang disapa sama dengan yang menyapa
            System.out.println("[DITOLAK] Objek yang sama tidak bisa saling menyapa");
            return;
        }
        ElemenFasilkom a = searchElemen(objek1);
        ElemenFasilkom b = searchElemen(objek2);
        for (ElemenFasilkom elemenFasilkom : a.getTelahMenyapa()) {
            if(elemenFasilkom==b){//menolak jika elemen yang disapa sudah disapa di hari yang sama
                System.out.println(String.format("[DITOLAK] %s telah menyapa %s hari ini", a,b));
                return;
            }
        }
        a.menyapa(b);
        b.menyapa(a);
        System.out.println(String.format("%s menyapa dengan %s", a,b));//berhasil menyapa elemen

        /* TODO: implementasikan kode Anda di sini */
    }

    public static void addMakanan(String objek, String namaMakanan, long harga) {// mekanisme menambah makanan ke etalase elemen kantin
        if (searchElemen(objek) instanceof ElemenKantin) {
            ElemenKantin kantin = (ElemenKantin)searchElemen(objek);
            kantin.setMakanan(namaMakanan, harga); //berhasil menambahkan makanan
        }else{
            System.out.println(String.format("[DITOLAK] %s bukan merupakan elemen kantin", objek));//tidak menemukan elemen kantin
        }

    }

    public static void membeliMakanan(String objek1, String objek2, String namaMakanan) {//mekanisme membeli makanan
        
        ElemenFasilkom a = searchElemen(objek1);
        ElemenFasilkom b = searchElemen(objek2);
        if (b instanceof ElemenKantin) {//pengecekan apakah penjual elemen kantin
            if (a != b) {
                a.membeliMakanan(a, b, namaMakanan);//elemen kantin tidak bisa membeli makanannya sendiri
            }else{
                System.out.println("[DITOLAK] Elemen kantin tidak bisa membeli makanan sendiri");
            }
        }else{
            System.out.println("[DITOLAK] Hanya elemen kantin yang dapat menjual makanan");
        }
        /* TODO: implementasikan kode Anda di sini */
    }

    public static void createMatkul(String nama, int kapasitas) {
        MataKuliah matkul = new MataKuliah(nama, kapasitas);
        daftarMataKuliah[totalMataKuliah++]=matkul;
        //menambahkan objek matkul ke dalam program
    }

    public static void addMatkul(String objek, String namaMataKuliah) {//mekanisme menambahkan matkul ke mahasiswa
        if(searchElemen(objek) instanceof Mahasiswa){
            Mahasiswa mhs = (Mahasiswa)searchElemen(objek);
            mhs.addMatkul(searchMatkkul(namaMataKuliah));//berhasil menambahkan matkul
        }else{
            System.out.println("[DITOLAK] Hanya mahasiswa yang dapat menambahkan matkul");//Jika objek bukan mahasiswa
        }
        /* TODO: implementasikan kode Anda di sini */
    }

    public static void dropMatkul(String objek, String namaMataKuliah) {//mekanisme dropmatkul
        if(searchElemen(objek) instanceof Mahasiswa){
            Mahasiswa mhs = (Mahasiswa)searchElemen(objek);
            mhs.dropMatkul(searchMatkkul(namaMataKuliah));
        }else{
            System.out.println("[DITOLAK] Hanya mahasiswa yang dapat drop matkul");//gagal dropmatkul karena objek buka mahasiwa
        }
        /* TODO: implementasikan kode Anda di sini */
    }

    public static void mengajarMatkul(String objek, String namaMataKuliah) {//mekanisme mengassign matkul ke dosen
        if (searchElemen(objek) instanceof Dosen) {
            Dosen dosen = (Dosen)searchElemen(objek);
            dosen.mengajarMataKuliah(searchMatkkul(namaMataKuliah));//berhasil mengassign dosen
        }else{
            System.out.println("[DITOLAK] Hanya dosen yang dapat mengajar matkul");//gagal karena objek bukan dosen
        }
        /* TODO: implementasikan kode Anda di sini */
    }

    public static void berhentiMengajar(String objek) {//mekanisme dropmatkul untuk dosen
        if (searchElemen(objek) instanceof Dosen) {
            Dosen dosen = (Dosen)searchElemen(objek);
            dosen.dropMataKuliah();//berhasil dropmatkul untuk dosen
        }else{
            System.out.println("[DITOLAK] Hanya dosen yang dapat berhenti mengajar");//gagal karena objek bukan dosen
        }
        /* TODO: implementasikan kode Anda di sini */
    }

    public static void ringkasanMahasiswa(String objek) {//mekanisme print ringkasan mahasiswa
        if (searchElemen(objek) instanceof Mahasiswa) {
            Mahasiswa mhs = (Mahasiswa)searchElemen(objek);
            System.out.println(String.format("Nama: %s \nTanggal lahir: %s\n Jurusan: %s ", 
            mhs,mhs.getTanggalLahir(),mhs.getJurusan()));                                       //print biodata mahasiwa
            MataKuliah[] daftarMataKuliah = mhs.getDaftarMatakuliah();

            System.out.println("Daftar Mata Kuliah:");
            if (mhs.getJumlahMatkul()==0) {
                System.out.println("Belum ada mata kuliah yang diambil");       //print matakuliah yang diambil mahasiswa
                return;
            }

            int counter = 1;
            for (MataKuliah mataKuliah : daftarMataKuliah) {
                if (mataKuliah!=null) {
                    System.out.println(String.format("%d. %s", counter++,mataKuliah));
                }
            }   
        }else{
                System.out.println("[DITOLAK] "+objek+" bukan merupakan seorang mahasiswa");
        }
    }

    public static void ringkasanMataKuliah(String namaMataKuliah) {//mekanisme ringkasan matakuliah
        MataKuliah matkul = searchMatkkul(namaMataKuliah);
        String dosen;
        if (matkul.getDosen()==null) {
            dosen = "Belum ada";
        }else{
            dosen = matkul.getDosen().toString();
        }

        System.out.println(String.format("Nama mata kuliah: %s\nJumlah mahasiswa: %d\nKapasitas: %d\nDosen pengajar: %s", 
        matkul,matkul.getJumlahMhs(),matkul.getKapasitas(),dosen));         //keterangan umum matkul

        System.out.println("Daftar mahasiswa yang mengambil mata kuliah ini: ");        //print daftar mahasiswa dalam matkul
        if (matkul.getJumlahMhs()==0) {
            System.out.println("Belum ada mahasiswa yang mengambil mata kuliah ini");
            return;
        }
        int counter = 1;
        for (Mahasiswa mahasiswa : matkul.getDaftarMahasiswa()) {
            if (mahasiswa!=null) {
                System.out.println(String.format("%d. %s", counter++,mahasiswa));
            }
        }
    }

    public static ElemenFasilkom resetMenyapa(int i) {  //method tambahan untuk memanggil resetmenyapa seluruh elemen menggunakan rekursi
        if (daftarElemenFasilkom[i]==null) {
            return null;
        }
        daftarElemenFasilkom[i].resetMenyapa();
        return resetMenyapa(i+1);
    }

    public static void nextDay() {// mekanisme nextday
        ElemenFasilkom[] tmp = Arrays.copyOf(daftarElemenFasilkom, totalElemenFasilkom);
        for (ElemenFasilkom elemenFasilkom : tmp) {
            if (elemenFasilkom.getJumlahMenyapa()>=(((float)totalElemenFasilkom-1)/2)) {
                elemenFasilkom.setFriendship(elemenFasilkom.getFriendship()+10);
            }else{
                elemenFasilkom.setFriendship(elemenFasilkom.getFriendship()-5);
            }
        }//menambahkan atau mengurangi friendship sesuai ketentuan

        System.out.println("Hari telah berakhir dan nilai friendship telah diupdate");
        friendshipRanking();
        resetMenyapa(0);
        /* TODO: implementasikan kode Anda di sini */
    }
    
    public static void friendshipRanking() {
        ElemenFasilkom[] tmp = Arrays.copyOf(daftarElemenFasilkom, totalElemenFasilkom);
        Arrays.sort(tmp);
        //memanfaatkan overriding method compareTo di elemen fasilkom 

        int counter=1; 
        for (ElemenFasilkom elemenFasilkom : tmp) {
            System.out.println(String.format("%d. %s(%d)", counter++,elemenFasilkom,elemenFasilkom.getFriendship()));
        }
    }

    public static void programEnd() {
        System.out.println("Program telah berakhir. Berikut nilai terakhir dari friendship pada Fasilkom :");
        friendshipRanking();
        //penyelesaian program dan menunjukan hasil akhir tingkat friendship mereka
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        while (true) {
            String in = input.nextLine();
            if (in.split(" ")[0].equals("ADD_MAHASISWA")) {
                addMahasiswa(in.split(" ")[1], Long.parseLong(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_DOSEN")) {
                addDosen(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("ADD_ELEMEN_KANTIN")) {
                addElemenKantin(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("MENYAPA")) {
                menyapa(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("ADD_MAKANAN")) {
                addMakanan(in.split(" ")[1], in.split(" ")[2], Long.parseLong(in.split(" ")[3]));
            } else if (in.split(" ")[0].equals("MEMBELI_MAKANAN")) {
                membeliMakanan(in.split(" ")[1], in.split(" ")[2], in.split(" ")[3]);
            } else if (in.split(" ")[0].equals("CREATE_MATKUL")) {
                createMatkul(in.split(" ")[1], Integer.parseInt(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_MATKUL")) {
                addMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("DROP_MATKUL")) {
                dropMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("MENGAJAR_MATKUL")) {
                mengajarMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("BERHENTI_MENGAJAR")) {
                berhentiMengajar(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MAHASISWA")) {
                ringkasanMahasiswa(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MATKUL")) {
                ringkasanMataKuliah(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("NEXT_DAY")) {
                nextDay();
            } else if (in.split(" ")[0].equals("PROGRAM_END")) {
                programEnd();
                input.close();
                break;
            }
        }
    }
}