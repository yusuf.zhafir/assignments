package assignments.assignment3;

class Dosen extends ElemenFasilkom {

    //variabel dosen

    MataKuliah mataKuliah;
    
    public MataKuliah getMatkul() {
        return mataKuliah;
    }

    public Dosen(String nama) {
        super(nama, "Dosen");
        //constructor
    }

    public void mengajarMataKuliah(MataKuliah mataKuliah) {//mekanisme mengassign maktul
        if(this.mataKuliah!=null){
            System.out.println(String.format("[DITOLAK] %s sudah mengajar mata kuliah %s", this,this.mataKuliah));
            return;
        }else if (mataKuliah.getDosen()!=null) {
            System.out.println(String.format("[DITOLAK] %s sudah memiliki dosen pengajar", mataKuliah));
            return;
        }
        this.mataKuliah=mataKuliah;
        mataKuliah.addDosen(this);
        System.out.println(String.format("%s mengajar mata kuliah %s", this,this.mataKuliah));   //berhasil mengassign matkul  
    }

    public void dropMataKuliah() {
        if (this.mataKuliah == null) {
            System.out.println(String.format("[DITOLAK] %s sedang tidak mengajar mata kuliah apapun", this));
            return;
        }
        System.out.println(String.format("%s berhenti mengajar %s", this, this.mataKuliah));
        this.mataKuliah.dropDosen();
        this.mataKuliah=null;
        //dropmatkul untuk dosen
    }
}