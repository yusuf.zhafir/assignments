package assignments.assignment3;

class MataKuliah {

    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    String nama;
    
    int kapasitas;

    int jumlahmhs;

    Dosen dosen;

    Mahasiswa[] daftarMahasiswa;

    public MataKuliah(String nama, int kapasitas) {//constructor
        this.nama=nama;
        this.kapasitas=kapasitas;
        daftarMahasiswa = new Mahasiswa[kapasitas];
        System.out.println(nama+" berhasil ditambahkan dengan kapasitas "+kapasitas);
    }

    //getter dan setter matakuliah

    public Mahasiswa[] getDaftarMahasiswa() {
        return daftarMahasiswa;
    }
    public Dosen getDosen() {
        return this.dosen;
    }

    public int getJumlahMhs() {
        return jumlahmhs;
    }
    public int getKapasitas() {
        return kapasitas;
    }

    public void addMahasiswa(Mahasiswa mahasiswa) {
        for (int i=0;i<daftarMahasiswa.length;i++) {
            if (daftarMahasiswa[i] == mahasiswa) {
                return;
            }else if (daftarMahasiswa[i] == null) {
                daftarMahasiswa[i] = mahasiswa;
                jumlahmhs++;
                return;
            }
        }
        //mekanisme addmahasiswa, akan gagal jika sudah ada mahasiswa
    }

    public void dropMahasiswa(Mahasiswa mahasiswa) {
        for (int i = 0; i < daftarMahasiswa.length; i++) {
            if(daftarMahasiswa[i]==mahasiswa){
                return;
            }
        }
        //mekanisme drop mahasiswa
    }

    public void addDosen(Dosen dosen) {
        this.dosen=dosen;
        //method mengassign dosen
    }

    public void dropDosen() {
        this.dosen=null;
        //method drop dosen
    }

    public String toString() {
        return this.nama;
    }
}