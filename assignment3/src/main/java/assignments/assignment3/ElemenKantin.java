package assignments.assignment3;


class ElemenKantin extends ElemenFasilkom {
    
    //variabel elemenkantin

    Makanan[] daftarMakanan = new Makanan[10];
    int jumlahPembeli;
    int counter;

    public ElemenKantin(String nama) {//constructor
        super(nama,"ElemenKantin");
    }

    public Makanan getMakanan(String nama) {//mencari makanan di daftar makanan
        for (Makanan makanan : daftarMakanan) {
            if(makanan!= null && makanan.toString().equals(nama)){
                return makanan;
            }
        }
        return null;
    }

    public void setMakanan(String nama, long harga) {//menambahkan makanan ke daftar
        if (getMakanan(nama)!=null) {
            System.out.println(String.format("[DITOLAK] %s sudah pernah terdaftar", nama));//gagal karena sudah ada makanan
            return;
        }
        Makanan mkn = new Makanan(nama, harga);
        daftarMakanan[counter]=mkn;
        counter++;
        System.out.println(String.format("%s telah mendaftarkan makanan %s dengan harga %d", toString(),nama,harga));
        //berhasil menambahkan makanan
    }
    
}