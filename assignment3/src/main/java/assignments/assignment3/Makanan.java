package assignments.assignment3;

class Makanan {

    //variabel makanan
    String nama;
    long harga;
    
    public long getHarga() {//getter harga
        return harga;
    }

    public Makanan(String nama, long harga) {//constructor
        this.harga=harga;
        this.nama=nama;
    }

    public String toString() {
        return this.nama;
    }
}