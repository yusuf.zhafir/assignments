package assignments.assignment3;

abstract class ElemenFasilkom implements Comparable<ElemenFasilkom>{
    
    //variabel class elemenfasilkom

    protected String tipe;
    
    protected String nama;

    protected int friendship;

    protected ElemenFasilkom[] telahMenyapa = new ElemenFasilkom[100];

    protected int counter;

    //method setter dan getter

    public String getNama() {
        return nama;
    }
    public ElemenFasilkom[] getTelahMenyapa() {
        return telahMenyapa;
    }
    public String getTipe() {
        return tipe;
    }
    public int getFriendship() {
        return friendship;
    }
    public int getJumlahMenyapa() {
        return counter;
    }
    public void setFriendship(int a) {
        if (a>100) {
            friendship=100;
        }else if (a<0) {
            friendship=0;
        }else{
            friendship = a;
        }
    }

    public ElemenFasilkom(String nama , String tipe){ //constructor
        this.nama = nama;
        this.tipe = tipe;
        System.out.println(nama+" berhasil ditambahkan");
    }

    public ElemenFasilkom searchTelahMenyapa(ElemenFasilkom elemenFasilkom) {//mencari elemen yang sudah di sapa dalam array telahmenyapa
        for (ElemenFasilkom i : telahMenyapa) {
            if (i==elemenFasilkom) {
                return i;
            }
        }return null;
    }
    
    public void menyapa(ElemenFasilkom elemenFasilkom) {//method menyapa
        telahMenyapa[counter++] = elemenFasilkom;
    }

    public void resetMenyapa() {//method reset menyapa
        this.telahMenyapa = new ElemenFasilkom[telahMenyapa.length];
        counter=0;
    }

    public void membeliMakanan(ElemenFasilkom pembeli, ElemenFasilkom penjual, String namaMakanan) {//method membeli makanan
        ElemenKantin abang = (ElemenKantin)penjual;
        Makanan mkn = abang.getMakanan(namaMakanan);
        if (mkn != null) {
            System.out.println(String.format("%s berhasil membeli %s seharga %d", pembeli,namaMakanan,mkn.getHarga()));
            abang.setFriendship(abang.getFriendship()+1);
            pembeli.setFriendship(pembeli.getFriendship()+1);
            return;
            //ketika berhasil membeli makanan secara otomatis tingkat friendship bertambah
        }
        System.out.println(String.format("[DITOLAK] %s tidak menjual %s", penjual,namaMakanan));
        //gagal membeli makanan
    }

    public String toString() {
        return this.nama;
    }
    @Override public int compareTo(ElemenFasilkom kiw) {
        int friendly = kiw.getFriendship();
        if (friendly==this.friendship) {
            return this.toString().compareTo(kiw.toString());
            //apabila tingkat friendship sama maka akan diurutkan sesuai abjad
        }
        return friendly-this.friendship;
        //menentukan perbandingan nilai antar elemen berdasarkan tingkat frienship
    }
}