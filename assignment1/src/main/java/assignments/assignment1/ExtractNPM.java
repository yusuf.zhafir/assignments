package assignments.assignment1;

import java.util.Scanner;

public class ExtractNPM {
    /*
    You can add other method do help you solve
    this problem
    
    Some method you probably need like
    - Method to get tahun masuk or else
    - Method to help you do the validation
    - and so on
    */

    public static boolean validate(long npm) {
        // TODO: validate NPM, return it with boolean
        String b = Long.toString(npm).substring(2, 4);
        String c = Long.toString(npm).substring(4, 12);
        if (checkB(b)!="salah" && checkC(c)!="salah" && checkE2(checkE1(Long.toString(npm).substring(0, 13)))==npm%10){         //mengecek validasi string dengan memanggil methon lain
            return true;
        }
        return false;
    }

    public static String checkB(String npm) {                       //pengecekan kode jurusan , ketika salah mereturn string "salah"
        int num= Integer.parseInt(npm);
        switch (num) {
            case 1 : return "Ilmu Komputer";
            case 2 : return "Sistem Informasi";
            case 3 : return "Teknologi Informasi";
            case 11 : return "Teknik Telekomunikasi";
            case 12 : return "Teknik Elektro";
        }
        return "salah";
    }

    public static int checkE1(String npm) {                             //mengecek npm terakhir menggunakan rekursi , perkalian string awal dan akhir
        if (npm.length()<2){
            return 0;
        }
        else{
            System.out.println(Integer.parseInt(npm.substring(0,1)) + " "+Integer.parseInt(npm.substring(npm.length()-1)));
            //System.out.println(Integer.parseInt(npm.substring(0,1)));
            return (Integer.parseInt(npm.substring(0,1))
            *Integer.parseInt(npm.substring(npm.length()-1)))
            +checkE1(npm.substring(1, npm.length()-1));
        }
    }

    public static int checkE2(int npm) {                                //mengecek npm terakhir menggunakan rekursi , menambahkan angka yang ada 
        while (npm>10){
            npm = (npm%10)+(npm/10);
        } 
        System.out.println(npm);
        return npm;
    }


    public static String checkC(String npm) {
        int day = Integer.parseInt(npm.substring(0, 2));                            //mengecek tanggal , apabila sebuah tanggal valid atau tidak
        int month = Integer.parseInt(npm.substring(2, 4));
        
        if (month == 2 && day>28){
            return "salah";
        }
        return npm.substring(0, 2)+"-"+npm.substring(2, 4)+"-"+npm.substring(4);
    }

    public static String extract(long npm) {                                                //extract string dengan memanfaatkan stringbuilder().append()
        // TODO: Extract information from NPM, return string with given format
        return new StringBuilder()
        .append("Tahun masuk: 20"+Long.toString(npm).substring(0, 2)+"\n")
        .append("Jurusan: "+checkB(Long.toString(npm).substring(2, 4))+"\n")
        .append("Tanggal Lahir: "+checkC(Long.toString(npm).substring(4, 12))).toString();
    }

    public static void main(String args[]) {
        Scanner input = new Scanner(System.in);
        boolean exitFlag = false;
        while (!exitFlag) {
            long npm = input.nextLong();
            
            if (npm < Math.pow(10, 13)) {
                System.out.println("NPM tidak valid!");
                exitFlag = true;
                break;
            }
            if (validate(npm)){                                                 //proses I/O
                System.out.println(extract(npm));
            }
            else{
                System.out.println("NPM tidak valid!");
            }
            
            // TODO: Check validate and extract NPM          
        }
        input.close();
    }
}
class A {}
