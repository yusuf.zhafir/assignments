package assignments.assignment2;

public class Mahasiswa {
    private MataKuliah[] mataKuliah = new MataKuliah[10];
    private int numMataKuliah;
    private String[] masalahIRS = new String[1];                            // instance variabel
    private int totalSKS;
    private String nama;
    private String jurusan;
    private long npm;

    public Mahasiswa(String nama, long npm){
        this.nama = nama;
        this.npm = npm;
        String b = Long.toString(npm).substring(2, 4);                  //constructor
        if (b.equals("01")) {
            this.jurusan = "Ilmu Komputer";
            
        }else{
            this.jurusan = "Sistem Informasi";
        }
        /* TODO: implementasikan kode Anda di sini */
    }
    public long getNPM() {
        return this.npm;
    }
    public String getNama() {
        return this.nama;
    }
    public String getJurusan() {
        return this.jurusan;
    }
    public int getNumMatkul() {                                         // getter
        return this.numMataKuliah;
    }
    public int getTotalSKS() {
        return this.totalSKS;
    }
    public String[] getMasalahIRS() {
        return this.masalahIRS;
    }
    public MataKuliah getMataKuliah(String namaMataKuliah) {
        for (int i = 0;i<10; i++) {
            if (mataKuliah[i] != null && mataKuliah[i].getNama().equals(namaMataKuliah)) {              //getter matkul
                return mataKuliah[i];
            }
        }return null;
    }
    public MataKuliah[] getDaftarMataKuliah() {                                                 //getter array
        return this.mataKuliah;
    }
    public void addMatkul(MataKuliah mataKuliah){                                               //setter matkul
        for (int i = 0;i<10;i++){
            if (this.mataKuliah[i]==null) {
                this.mataKuliah[i] = mataKuliah;
                this.totalSKS += mataKuliah.getSKS(); 
                numMataKuliah++;
                return;
            }
        }
        /* TODO: implementasikan kode Anda di sini */
    }

    public void dropMatkul(MataKuliah mataKuliah){                                      //setter untuk drop matkul
        for (int i = 0;i<10;i++){
            if (this.mataKuliah[i]==mataKuliah) {
                this.mataKuliah[i] = null;
                this.numMataKuliah--;
                this.totalSKS -= mataKuliah.getSKS();
                return;
            }
        }
        /* TODO: implementasikan kode Anda di sini */
    }


    public void cekIRS(){       
        this.masalahIRS = new String[1];                                    //menginisiasi array baru setiap mengecek array
        for(int i = 0;i<10;i++){
            if (this.mataKuliah[i]!=null) {
                if(this.jurusan.equals("Ilmu Komputer") && this.mataKuliah[i].getKode().equals("SI")){  //memanggil method append array untuk memasukan kedalam array irs
                    appendArrayMasalahIRSKosong(String.format("Mata Kuliah %s tidak dapat diambil jurusan IK\n", this.mataKuliah[i].getNama()));
                }else if (this.jurusan.equals("Sistem Informasi") && this.mataKuliah[i].getKode().equals("IK")) {
                    appendArrayMasalahIRSKosong(String.format("Mata Kuliah %s tidak dapat diambil jurusan SI\n",this.mataKuliah[i].getNama()));
                }
                if (this.totalSKS>24) {
                    appendArrayMasalahIRSKosong(String.format("SKS yang Anda ambil lebih dari 24"));
                }
            }
        }
        /* TODO: implementasikan kode Anda di sini */
    }
    public void appendArrayMasalahIRSKosong(String masalah) {                   //method append array untuk penambahan masalah
        String[] temp = new String[this.masalahIRS.length+1];
        for (int i = 0; i < this.masalahIRS.length; i++) {
            temp[i] = this.masalahIRS[i];
        }
        for (int i = 0; i < temp.length+1; i++) {
            if(temp[i]==null){
                temp[i] = masalah;
                this.masalahIRS = temp;
            }
            if (this.masalahIRS[i].equals(masalah)) {
                return;
            }
        } 
    }

    public String toString() {
        /* TODO: implementasikan kode Anda di sini */
        return this.nama;
    }

}
