package assignments.assignment2;

public class MataKuliah {
    private String kode;
    private String nama;
    private int sks;
    private int kapasitas;
    private Mahasiswa[] daftarMahasiswa;                                        //instance variabel
    private int numMahasiswa;

    public MataKuliah(String kode, String nama, int sks, int kapasitas){        //constructor
        /* TODO: implementasikan kode Anda di sini */
        this.kode = kode;
        this.nama = nama;
        this.sks = sks;
        this.kapasitas = kapasitas;
        this.daftarMahasiswa = new Mahasiswa[kapasitas];

    }

    public String getKode() {
        return this.kode;
    }
    public int getSKS() {
        return this.sks;
    }
    public String getNama() {
        return this.nama;
    }
    public int getKapasitas() {                                             //getter
        return this.kapasitas;
    }
    public int getNumMahasiswa() {
        return this.numMahasiswa;
    }
    public Mahasiswa[] getDaftarMahasiswa() {
        return this.daftarMahasiswa;
    }


    public void addMahasiswa(Mahasiswa mahasiswa) {
        for (int i = 0;i<this.kapasitas;i++) {
            if (this.daftarMahasiswa[i] == null) {
                this.daftarMahasiswa[i] = mahasiswa;                                        //setter array mahasiswa
                this.numMahasiswa++;
                return;
            }
        }
        /* TODO: implementasikan kode Anda di sini */

    }

    public void dropMahasiswa(Mahasiswa mahasiswa) {
        for (int i=0;i<this.kapasitas;i++) {
            if (this.daftarMahasiswa[i] == mahasiswa) {                                     //setter
                this.daftarMahasiswa[i] = null;
                numMahasiswa--;
                return ;
            }
        }
        /* TODO: implementasikan kode Anda di sini */

    }

    public String toString() {
        /* TODO: implementasikan kode Anda di sini */
        return this.nama;
    }
}
