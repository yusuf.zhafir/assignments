package assignments.assignment3;


class Mahasiswa extends ElemenFasilkom {
    
    //variabel untuk mahasiswa

    MataKuliah[] daftarMataKuliah = new MataKuliah[10];
    int jumlahmatkul;
    long npm;
    String tanggalLahir;
    String jurusan;

    //getter dan setter

    public String getTanggalLahir() {
        return tanggalLahir;
    }
    public String getJurusan() {
        return jurusan;
    }
    public MataKuliah[] getDaftarMatakuliah() {
        return daftarMataKuliah;
    }
    public long getNpm() {
        return npm;
    }
    public int getJumlahMatkul() {
        return jumlahmatkul;
    }
    public Mahasiswa(String nama, long npm) {//constructor
        super(nama, "Mahasiswa");
        this.npm=npm;
        jurusan = extractJurusan(npm);
        tanggalLahir = extractTanggalLahir(npm);
    }

    public MataKuliah searcMataKuliah(MataKuliah mataKuliah) {
        for (MataKuliah matkul : daftarMataKuliah) {                        //method untuk mencari matkul yang ada di daftar matkul
            if (matkul==mataKuliah) {
                return matkul;
            }            
        }return null;
    }

    @Override
    public void menyapa(ElemenFasilkom elemenFasilkom) {
        super.menyapa(elemenFasilkom);
        if (elemenFasilkom instanceof Dosen) {
            Dosen dosen = (Dosen)elemenFasilkom;
            if (dosen.getMatkul()!=null && dosen.getMatkul()==searcMataKuliah(dosen.getMatkul())) {
                dosen.setFriendship(dosen.getFriendship()+2);
                setFriendship(getFriendship()+2);
            }
        }//mengoverride untuk menambahkan nilai friendship langsung saat interaksi menyapa dosen
    }

    public void addMatkul(MataKuliah mataKuliah) {//mekanisme add matkul
        if (searcMataKuliah(mataKuliah)==null) {
            if (mataKuliah.getKapasitas()>mataKuliah.getJumlahMhs()) {
                for (int i = 0; i < daftarMataKuliah.length; i++) {
                    if (daftarMataKuliah[i]==null) {
                        mataKuliah.addMahasiswa(this);
                        daftarMataKuliah[i]=mataKuliah;
                        jumlahmatkul++;
                        System.out.println(String.format("%s berhasil menambahkan mata kuliah %s", toString(),mataKuliah));
                        return;
                        //berhasil menambahkan matkul
                    }
                }
            }else{
                System.out.println(String.format("[DITOLAK] %s telah penuh kapasitasnya", mataKuliah));//gagal karena kapasitas sudah penuh
            }
        }else{
            System.out.println(String.format("[DITOLAK] %s telah diambil sebelumnya", mataKuliah));//gagal karena sudah pernah diambil
        }
        /* TODO: implementasikan kode Anda di sini */
    }

    public void dropMatkul(MataKuliah mataKuliah) {
        if (searcMataKuliah(mataKuliah)!=null) {
            for (int i = 0; i < daftarMataKuliah.length; i++) {
                if (daftarMataKuliah[i]==mataKuliah) {
                    daftarMataKuliah[i].dropMahasiswa(this);
                    daftarMataKuliah[i]=null;
                    jumlahmatkul--;
                    System.out.println(String.format("%s berhasil drop mata kuliah %s", this,mataKuliah));
                    return;
                    //berhasil drop matkul
                }
            }
        }else{
            System.out.println(String.format("[DITOLAK] %s belum pernah diambil", mataKuliah));
            //gagal drop matkul karena belum pernah diambil
        }
    }

    public String extractTanggalLahir(long npm) {
        String tanggal = String.valueOf(Integer.parseInt(Long.toString(npm).substring(4, 6)));
        String bulan = String.valueOf(Integer.parseInt(Long.toString(npm).substring(6, 8)));
        String tahun = String.valueOf(Integer.parseInt(Long.toString(npm).substring(8, 12)));
        return String.format("%s-%s-%s", tanggal,bulan,tahun);
        //mengambil tanggal lahir dari npm
    }

    public String extractJurusan(long npm) {
        String b = Long.toString(npm).substring(2, 4);                  //constructor
        if (b.equals("01")) {
            return "Ilmu Komputer";
            
        }else{
            return "Sistem Informasi";
        }
        //menentukan jurusan dari npm
    }
}